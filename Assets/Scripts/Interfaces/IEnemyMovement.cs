﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyMovement
{
    void Move(Vector3 direction);
    void Stop(float timer);
    void StartMovement();
    void Jump(float height);
}
