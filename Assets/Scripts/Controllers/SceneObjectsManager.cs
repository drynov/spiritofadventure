﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneObjectsManager : MonoBehaviour
{
    public static SceneObjectsManager Instance { get; private set; }

    public List<IEnemy> Enemies = new List<IEnemy>();
    public List<IBonus> Bonuses = new List<IBonus>();
    public List<INeutral> Substances = new List<INeutral>();

    private bool EnemiesReady;
    private bool BonusesReady;

    private bool isCheckingEnemies;
    private bool isCheckingBonuses;
    private bool isCheckingSubstances;

    public Action OnEnemySpawnAction;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    // Use this for initialization
    void Start()
    {
        GameStateMachine.Instance.OnPlayerTurnAction += ResetObjectStatus;
        GameStateMachine.Instance.OnEnemyTurnAction += CheckBonuses;
        GameStateMachine.Instance.OnEnemyTurnAction += CheckEnemies;
        GameStateMachine.Instance.OnOtherObjectAction += CheckSubstances;
    }

    private void OnDestroy()
    {
        GameStateMachine.Instance.OnPlayerTurnAction -= ResetObjectStatus;
        GameStateMachine.Instance.OnEnemyTurnAction -= CheckBonuses;
        GameStateMachine.Instance.OnEnemyTurnAction -= CheckEnemies;
        GameStateMachine.Instance.OnOtherObjectAction -= CheckSubstances;
    }

    private void ResetObjectStatus()
    {
        EnemiesReady = false;
        BonusesReady = false;
    }

    public void Registrate(IEnemy enemy)
    {
        if (!Enemies.Contains(enemy))
        {
            Enemies.Add(enemy);
            if (OnEnemySpawnAction != null)
                OnEnemySpawnAction();
        }
    }

    public void Registrate(IBonus bonus)
    {
        if (!Bonuses.Contains(bonus))
            Bonuses.Add(bonus);
    }

    public void Registrate(INeutral substance)
    {
        if (!Substances.Contains(substance))
            Substances.Add(substance);
    }

    public void UnRegistrate(IEnemy enemy)
    {
        if (Enemies.Contains(enemy))
            Enemies.Remove(enemy);
        if (Enemies.Count <= 0)
        {
            if (SceneManager.GetActiveScene().name == "Tutorial_SampleScene")
            {
                return;
            }
            if (LevelRoundLoader.Instance.isLastRound && SceneManagerScript.Instance.player.GetComponent<PlayerHealth>().HP > 0)
            {
                if (SceneManagerScript.Instance.OnVictoryAction != null)
                    SceneManagerScript.Instance.OnVictoryAction();
            }
            else
            {
                if (BallsManager.Instance.ActiveBalls.Count <= 0 && GameStateMachine.Instance.CurrentState == GameStates.PlayerTurn)
                {
                    StartCoroutine(ChangeTurnCoroutine(GameStates.EnemyTurn));
                }
            }
        }
    }

    private IEnumerator ChangeTurnCoroutine(GameStates state)
    {
        yield return new WaitForSeconds(0.1f);
        GameStateMachine.Instance.ChangeState(state);
    }

    public void UnRegistrate(IBonus bonus)
    {
        if (Bonuses.Contains(bonus))
            Bonuses.Remove(bonus);
        if (Bonuses.Count <= 0)
        {
            UpdateBonusesStatus();
        }
    }

    public void UnRegistrate(INeutral substance)
    {
        if (Substances.Contains(substance))
            Substances.Remove(substance);
    }

    private void CheckEnemies()
    {
        if (Enemies.Count <= 0)
        {
            print("Enemies CHECKED!");
            EnemiesReady = true;
            StageReadyCheck();
        }
    }

    public void UpdateEnemiesStatus()
    {
        if (isCheckingEnemies)
            return;
        isCheckingEnemies = true;
        if (Enemies.Count > 0)
            StartCoroutine(CheckEnemiesCoroutine());
        else
        {
            print("Enemies CHECKED!");
            EnemiesReady = true;
            StageReadyCheck();
            isCheckingEnemies = false;
        }
    }

    private void CheckBonuses()
    {
        if (Bonuses.Count <= 0)
        {
            print("Bonuses CHECKED!");
            BonusesReady = true;
            StageReadyCheck();
        }
    }

    public void UpdateBonusesStatus()
    {
        if (isCheckingBonuses)
            return;
        isCheckingBonuses = true;
        if (Bonuses.Count > 0)
            StartCoroutine(CheckBonusesCoroutine());
        else
        {
            print("Bonuses CHECKED!");
            BonusesReady = true;
            StageReadyCheck();
            isCheckingBonuses = false;
        }
    }

    private void StageReadyCheck()
    {
        if (EnemiesReady && BonusesReady)
        {
            GameStateMachine.Instance.ChangeState(GameStates.OtherObjectsTurn);
            EnemiesReady = false;
            BonusesReady = false;
        }
    }

    private void CheckSubstances()
    {
        if (Substances.Count <= 0)
        {
            print("Substances CHECKED!");
            if (LevelRoundLoader.Instance && !LevelRoundLoader.Instance.isLastRound)
                GameStateMachine.Instance.ChangeState(GameStates.SceneBuilding);
            else
                GameStateMachine.Instance.ChangeState(GameStates.PlayerTurn);
        }
    }

    public void UpdateSubstancesStatus()
    {
        if (isCheckingSubstances)
            return;
        isCheckingSubstances = true;
        if (Substances.Count > 0)
            StartCoroutine(CheckSubstancesCoroutine());
        else
        {
            isCheckingSubstances = false;
            print("Substances CHECKED!");
            if (!LevelRoundLoader.Instance.isLastRound)
                GameStateMachine.Instance.ChangeState(GameStates.SceneBuilding);
            else
                GameStateMachine.Instance.ChangeState(GameStates.PlayerTurn);
        }
    }

    private IEnumerator CheckEnemiesCoroutine()
    {
        bool isChecking = true;
        while (isChecking)
        {
            foreach (var enemy in Enemies)
            {
                isChecking = false;
                if (!enemy.ReadyCheck())
                {
                    isChecking = true;
                    break;
                }
            }
            print("Keep checking enemies");
            yield return new WaitForSeconds(0.1f);
        }
        isCheckingEnemies = false;
        print("ENEMIES CHECKED!");
        EnemiesReady = true;
        StageReadyCheck();
    }

    private IEnumerator CheckBonusesCoroutine()
    {
        bool isChecking = true;
        while (isChecking)
        {
            foreach (var bonus in Bonuses)
            {
                isChecking = false;
                if (!bonus.ReadyCheck())
                {
                    isChecking = true;
                    break;
                }
            }
            print("Keep checking bonuses");
            yield return null;
        }
        isCheckingBonuses = false;
        print("BONUSES CHECKED!");
        BonusesReady = true;
        StageReadyCheck();
    }

    private IEnumerator CheckSubstancesCoroutine()
    {
        bool isChecking = true;
        while (isChecking)
        {
            foreach (var substance in Substances)
            {
                isChecking = false;
                if (!substance.ReadyCheck())
                {
                    isChecking = true;
                    break;
                }
            }
            print("Keep checking substances");
            yield return null;
        }
        isCheckingSubstances = false;
        print("SUBSTANCES CHECKED!");
        if (!LevelRoundLoader.Instance.isLastRound)
        {
            if (GameStateMachine.Instance.CurrentState == GameStates.OtherObjectsTurn)
                GameStateMachine.Instance.ChangeState(GameStates.SceneBuilding);
        }
        else
        {
            if (GameStateMachine.Instance.CurrentState == GameStates.OtherObjectsTurn)
                GameStateMachine.Instance.ChangeState(GameStates.PlayerTurn);
        }
    }
}
