﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GameStates { SceneBuilding, PlayerTurn, EnemyTurn, OtherObjectsTurn, Undefined }
public class GameStateMachine : MonoBehaviour
{
    public static GameStateMachine Instance { get; private set; }

    public Action OnChangeStateAction;
    public Action OnSceneBuildingAction;
    public Action OnPlayerTurnAction;
    public Action OnEnemyTurnAction;
    public Action OnOtherObjectAction;

    public GameStates CurrentState { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    // Use this for initialization
    void Start()
    {
        ChangeState(GameStates.SceneBuilding);
    }

    public void ChangeState(GameStates state)
    {
        if (CurrentState != state)
        {
            print("It was " + CurrentState + " and now - " + state);
            CurrentState = state;
            if (OnChangeStateAction != null)
                OnChangeStateAction();
            switch (CurrentState)
            {
                case GameStates.SceneBuilding:
                    {
                        if (OnSceneBuildingAction != null)
                            OnSceneBuildingAction();
                    }
                    break;
                case GameStates.PlayerTurn:
                    {
                        if (OnPlayerTurnAction != null)
                            OnPlayerTurnAction();
                    }
                    break;
                case GameStates.EnemyTurn:
                    {
                        if (OnEnemyTurnAction != null)
                            OnEnemyTurnAction();
                    }
                    break;
                case GameStates.OtherObjectsTurn:
                    {
                        if (OnOtherObjectAction != null)
                            OnOtherObjectAction();
                    }
                    break;
                case GameStates.Undefined:
                    break;
                default:
                    break;
            }
        }
    }
}
