﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerScript : MonoBehaviour
{
    public static SceneManagerScript Instance { get; private set; }

    [SerializeField]
    public GameObject m_gameCanvas;
    [SerializeField]
    public Camera m_mainCamera;
    [SerializeField]
    public Transform m_coinCollectPoint;

    [SerializeField]
    private AudioClip m_objectsMovementAudioClip;
    [SerializeField]
    private AudioClip m_objectsAppearingAudioClip;

    public PlayerController player;

    public Action<BoxCollider> OnZoneChangeAction;
    public Action<int> UpdateMoney;
    public Action OnLoseAction;
    public Action OnVictoryAction;
    public Action OnLevelBuilt;

    private BoxCollider walkingZone;
    private IEnumerator myCoroutine;

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
    }

    private void Start()
    {
        if (CustomAnalytics.Instance)
        {
            OnLevelBuilt += CustomAnalytics.Instance.StartLevel;
            OnVictoryAction += CustomAnalytics.Instance.CompleteLevel;
            OnLoseAction += CustomAnalytics.Instance.FailedLevel;
        }
        GameStateMachine.Instance.OnEnemyTurnAction += EnemyTurnMoveSound;
        GameStateMachine.Instance.OnSceneBuildingAction += EnemyAppearSound;
        AudioSource.PlayClipAtPoint(m_objectsAppearingAudioClip, m_mainCamera.transform.position);
    }

    private void OnDestroy()
    {
        if (CustomAnalytics.Instance)
        {
            OnLevelBuilt -= CustomAnalytics.Instance.StartLevel;
            OnVictoryAction -= CustomAnalytics.Instance.CompleteLevel;
            OnLoseAction -= CustomAnalytics.Instance.FailedLevel;
        }
        GameStateMachine.Instance.OnEnemyTurnAction -= EnemyTurnMoveSound;
        GameStateMachine.Instance.OnSceneBuildingAction -= EnemyAppearSound;
    }
    
    private void EnemyTurnMoveSound()
    {
        AudioSource.PlayClipAtPoint(m_objectsMovementAudioClip, m_mainCamera.transform.position);
    }

    private void EnemyAppearSound()
    {
        AudioSource.PlayClipAtPoint(m_objectsMovementAudioClip, m_mainCamera.transform.position);
    }

    [ContextMenu("Debug WIN LEVEL")]
    public void SetVictory()
    {
        if (OnVictoryAction != null)
        {
            OnVictoryAction();
        }
    }

    public BoxCollider WalkingZone
    {
        get { return walkingZone; }
        set
        {
            walkingZone = value;
            if (OnZoneChangeAction != null)
            {
                OnZoneChangeAction(walkingZone);
            }
        }
    }
}