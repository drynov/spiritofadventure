﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LevelBuildingManager : EditorWindow
{
    private GeneratedLevelData generatedLevelData;
    private GeneratedLevelData loadedLevelData;
    private GeneratedRound currentGeneratedRound;
    private List<GeneratedRound> levelRounds = new List<GeneratedRound>();
    private List<GameObject> groundElements = new List<GameObject>();
    private LevelBalanceData levelBalanceData;

    private string loadedFilePath;
    private int startWaves = 1;

    [MenuItem("Tools/Level Building Manager")]
    public static void ShowWindow()
    {
        GetWindow(typeof(LevelBuildingManager));
    }

    private void OnGUI()
    {
        GUILayout.Label("Generate level", EditorStyles.boldLabel);

        if (GUILayout.Button("Add ground"))
        {
            AddGround();
        }

        if (GUILayout.Button("Remove ground"))
        {
            RemoveGround();
        }

        GUILayout.Label("Start Waves Count", EditorStyles.boldLabel);

        startWaves = EditorGUILayout.IntField("Waves Count", startWaves);

        GUILayout.Label("Save level", EditorStyles.boldLabel);

        if (GUILayout.Button("Save level data"))
        {
            SaveRoundsData();
        }

        GUILayout.Label("Load level", EditorStyles.boldLabel);
        if (GUILayout.Button("Load level data"))
        {
            LoadLevelData();
        }
    }

    private void AddGround()
    {
        ClearGroundList();
        GameObject levelBase = Instantiate(Resources.Load<GameObject>("LevelPrefabs/LevelBase(for editor)"));
        groundElements.Add(levelBase);
        levelBase.transform.position += 9f * (groundElements.Count - 1) * Vector3.forward;
    }

    private void RemoveGround()
    {
        ClearGroundList();
        if (groundElements.Count > 0)
        {
            DestroyImmediate(groundElements[groundElements.Count - 1]);
            groundElements.RemoveAt(groundElements.Count - 1);
        }
    }

    private void LoadLevelData()
    {
        string filePath = EditorUtility.OpenFilePanel("Select level data file", Application.streamingAssetsPath, "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAstext = File.ReadAllText(filePath);
            loadedLevelData = JsonUtility.FromJson<GeneratedLevelData>(dataAstext);
        }
        startWaves = loadedLevelData.StartWavesCount;
        UpdateLevelData(loadedLevelData.BalanceData);
        BuildLoadedLevel(loadedLevelData);
    }

    private void BuildLoadedLevel(GeneratedLevelData levelData)
    {
        GameObject root = GameObject.FindGameObjectWithTag("LevelRoot");
        if (root)
            DestroyImmediate(root);

        root = new GameObject();
        root.transform.position = levelData.LevelRoot.Position;
        root.transform.rotation = levelData.LevelRoot.Rotation;
        root.name = levelData.LevelRoot.Name;
        root.tag = levelData.LevelRoot.Tag;

        for (int i = 0; i < levelData.Rounds.Length; i++)
        {
            if (levelData.Rounds[i].RoundItems == null || levelData.Rounds[i].RoundItems.Length == 0)
                continue;

            for (int j = 0; j < levelData.Rounds[i].RoundItems.Length; j++)
            {
                GameObject obj = Resources.Load<GameObject>("LevelPrefabs/" + levelData.Rounds[i].RoundItems[j].Marker);
                obj = Instantiate(obj, levelData.Rounds[i].RoundItems[j].Position, levelData.Rounds[i].RoundItems[j].Rotation, root.transform);
                obj.transform.localScale = levelData.Rounds[i].RoundItems[j].Scale;
                obj.name = levelData.Rounds[i].RoundItems[j].Name;
                //obj.tag = levelData.Rounds[i].RoundItems[j].Tag;
            }
        }


    }

    private void SaveLevelData(GameObject root)
    {
        root = GameObject.FindGameObjectWithTag("LevelRoot");
        generatedLevelData = new GeneratedLevelData();
        generatedLevelData.StartWavesCount = startWaves;
        generatedLevelData.LevelRoot = PreapreLevelItem(root);
        levelBalanceData = Resources.Load<LevelBalanceData>("ObjectsDatabases/LevelBalanceData");
        generatedLevelData.BalanceData = PreapreLevelBalance(levelBalanceData);
        generatedLevelData.Rounds = new GeneratedRound[levelRounds.Count];

        for (int i = 0; i < generatedLevelData.Rounds.Length; i++)
        {
            generatedLevelData.Rounds[i] = levelRounds[i];
        }

        string filePath = EditorUtility.SaveFilePanel("Save generated level data file", Application.streamingAssetsPath, "", "json");

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsText = JsonUtility.ToJson(generatedLevelData);
            File.WriteAllText(filePath, dataAsText);
            Debug.LogError("Successfully Saved!");
            AssetDatabase.Refresh();
        }
    }

    private void SaveRoundsData()
    {
        GameObject root = GameObject.FindGameObjectWithTag("LevelRoot");
        if (!root)
            return;

        levelRounds.Clear();
        List<GameObject> currentRoundObjects = new List<GameObject>();

        GameObject[] sceneObjects = new GameObject[root.transform.childCount];
        for (int i = 0; i < root.transform.childCount; i++)
        {
            sceneObjects[i] = root.transform.GetChild(i).gameObject;
        }
        sceneObjects = sceneObjects.OrderByDescending(go => go.transform.position.z).ToArray();

        int roundNumber = 0;
        int z = Mathf.RoundToInt(sceneObjects[0].transform.position.z);
        for (int i = -1; i <= z; i++)
        {
            currentRoundObjects.Clear();
            for (int j = 0; j < sceneObjects.Length; j++)
            {
                if (Mathf.Abs(sceneObjects[j].transform.position.z - i) < 0.2f)
                {
                    currentRoundObjects.Add(sceneObjects[j]);
                }
            }
            levelRounds.Add(new GeneratedRound());
            levelRounds[roundNumber].RoundItems = new GeneratedLevelItem[currentRoundObjects.Count];
            for (int k = 0; k < currentRoundObjects.Count; k++)
            {
                levelRounds[roundNumber].RoundItems[k] = PreapreLevelItem(currentRoundObjects[k]);
            }

            roundNumber++;
        }
        SaveLevelData(root);
    }

    private void UpdateLevelData(BalanceData data)
    {
        BallsData m_ballsData = Resources.Load<BallsData>("ObjectsDatabases/BallsDatabase");
        SceneObjectsData m_objectsData = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        PlayerCharacterData m_playerData = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData");
        LevelBalanceData m_balanceData = Resources.Load<LevelBalanceData>("ObjectsDatabases/LevelBalanceData");

        for (int i = 0; i < data.BallsDatabase.Length; i++)
        {
            for (int j = 0; j < m_ballsData.BallsDatabase.Length; j++)
            {
                if (data.BallsDatabase[i].Ball_Type == m_ballsData.BallsDatabase[j].Ball_Type)
                {
                    m_ballsData.BallsDatabase[i].Ball_damage = data.BallsDatabase[i].Damage;
                    m_ballsData.BallsDatabase[i].Ball_speed = data.BallsDatabase[i].Speed;
                }

                if (data.BallsDatabase[i].Ball_Type == m_balanceData.BallsDatabase[j].Ball_Type)
                {
                    m_balanceData.BallsDatabase[i].Damage = data.BallsDatabase[i].Damage;
                    m_balanceData.BallsDatabase[i].Speed = data.BallsDatabase[i].Speed;
                }
            }
        }

        for (int i = 0; i < data.ObjectsDatabase.Length; i++)
        {
            for (int j = 0; j < m_objectsData.ObjectsDatabase.Length; j++)
            {
                if (data.ObjectsDatabase[i].Object_Type == m_objectsData.ObjectsDatabase[j].Object_Type)
                {
                    m_objectsData.ObjectsDatabase[j].Object_damage = data.ObjectsDatabase[i].Damage;
                    m_objectsData.ObjectsDatabase[j].Object_health = data.ObjectsDatabase[i].Health;

                    switch (data.ObjectsDatabase[i].Object_Type)
                    {
                        case ObjectType.berry:
                            m_objectsData.ObjectsDatabase[j].Object_extraBallDuration = data.ObjectsDatabase[i].ExtraBallDuration;
                            m_objectsData.ObjectsDatabase[j].Object_extraBallType = data.ObjectsDatabase[i].ExtraBallType;
                            break;
                        case ObjectType.groot:
                            m_objectsData.ObjectsDatabase[j].Object_HealthRegen = data.ObjectsDatabase[i].HealthRegenAmount;
                            break;
                        case ObjectType.stoneCube:
                            break;
                        case ObjectType.bushCube:
                            break;
                        case ObjectType.bomb:
                            m_objectsData.ObjectsDatabase[j].Object_ExplosionDamage = data.ObjectsDatabase[i].ExplosionDamage;
                            m_objectsData.ObjectsDatabase[j].Object_ExplosionRadius = data.ObjectsDatabase[i].ExplosionRadius;
                            break;
                        case ObjectType.knight:
                            m_objectsData.ObjectsDatabase[j].Object_FrontShield = data.ObjectsDatabase[i].FrontShield;
                            m_objectsData.ObjectsDatabase[j].Object_BackShield = data.ObjectsDatabase[i].BackShield;
                            m_objectsData.ObjectsDatabase[j].Object_RightShield = data.ObjectsDatabase[i].RightShield;
                            m_objectsData.ObjectsDatabase[j].Object_LeftShield = data.ObjectsDatabase[i].LeftShield;
                            break;
                        case ObjectType.ram:
                            break;
                        case ObjectType.jelly:
                            m_objectsData.ObjectsDatabase[j].Object_FragmentsCount = data.ObjectsDatabase[i].FragmentsCount;
                            break;
                        case ObjectType.gunner:
                            m_objectsData.ObjectsDatabase[j].Object_ShotDamage = data.ObjectsDatabase[i].ShotDamage;
                            m_objectsData.ObjectsDatabase[j].Object_ShootDistance = data.ObjectsDatabase[i].ShootDistance;
                            break;
                        default:
                            break;
                    }
                }

                if (data.ObjectsDatabase[i].Object_Type == m_balanceData.ObjectsDatabase[j].Object_Type)
                {
                    m_balanceData.ObjectsDatabase[j].Damage = data.ObjectsDatabase[i].Damage;
                    m_balanceData.ObjectsDatabase[j].Health = data.ObjectsDatabase[i].Health;

                    switch (data.ObjectsDatabase[i].Object_Type)
                    {
                        case ObjectType.berry:
                            m_balanceData.ObjectsDatabase[j].ExtraBallType = data.ObjectsDatabase[i].ExtraBallType;
                            m_balanceData.ObjectsDatabase[j].ExtraBallDuration = data.ObjectsDatabase[i].ExtraBallDuration;
                            break;
                        case ObjectType.groot:
                            m_balanceData.ObjectsDatabase[j].HealthRegenAmount = data.ObjectsDatabase[i].HealthRegenAmount;
                            break;
                        case ObjectType.stoneCube:
                            break;
                        case ObjectType.bushCube:
                            break;
                        case ObjectType.bomb:
                            m_balanceData.ObjectsDatabase[j].ExplosionDamage = data.ObjectsDatabase[i].ExplosionDamage;
                            m_balanceData.ObjectsDatabase[j].ExplosionRadius = data.ObjectsDatabase[i].ExplosionRadius;
                            break;
                        case ObjectType.knight:
                            m_balanceData.ObjectsDatabase[j].FrontShield = data.ObjectsDatabase[i].FrontShield;
                            m_balanceData.ObjectsDatabase[j].BackShield = data.ObjectsDatabase[i].BackShield;
                            m_balanceData.ObjectsDatabase[j].RightShield = data.ObjectsDatabase[i].RightShield;
                            m_balanceData.ObjectsDatabase[j].LeftShield = data.ObjectsDatabase[i].LeftShield;
                            break;
                        case ObjectType.ram:
                            break;
                        case ObjectType.jelly:
                            m_balanceData.ObjectsDatabase[j].FragmentsCount = data.ObjectsDatabase[i].FragmentsCount;
                            break;
                        case ObjectType.gunner:
                            m_balanceData.ObjectsDatabase[j].ShotDamage = data.ObjectsDatabase[i].ShotDamage;
                            m_balanceData.ObjectsDatabase[j].ShootDistance = data.ObjectsDatabase[i].ShootDistance;
                            break;
                        default:
                            break;
                    }
                }

            }
        }

        m_balanceData.PlayerLifes = data.PlayerLifes;
        m_balanceData.BallsPerTurn = data.BallsPerTurn;
        m_balanceData.BetweenBallsTimer = data.BetweenBallsTimer;
        m_playerData.PlayerLifes = data.PlayerLifes;
        m_playerData.BallsPerTurn = data.BallsPerTurn;
        m_playerData.BetweenBallsTimer = data.BetweenBallsTimer;
        m_playerData.CoinsPerStar = data.CoinsInChest;
    }

    private GeneratedLevelItem PreapreLevelItem(GameObject obj)
    {
        GeneratedLevelItem item = new GeneratedLevelItem();
        LevelEditorObject editorObj = obj.GetComponent<LevelEditorObject>();
        if (editorObj)
        {
            item.Marker = editorObj.Marker;
        }
        item.Name = obj.name;
        item.Tag = obj.tag;
        item.Position = obj.transform.position;
        item.Rotation = obj.transform.rotation;
        item.Scale = obj.transform.localScale;
        return item;
    }

    private BalanceData PreapreLevelBalance(LevelBalanceData data)
    {
        BalanceData balance = new BalanceData();
        balance.BallsPerTurn = data.BallsPerTurn;
        balance.BetweenBallsTimer = data.BetweenBallsTimer;
        balance.PlayerLifes = data.PlayerLifes;
        balance.CoinsInChest = data.CoinsInChest;
        ObjectShortData[] objects = new ObjectShortData[data.ObjectsDatabase.Length];
        for (int i = 0; i < data.ObjectsDatabase.Length; i++)
        {
            objects[i] = new ObjectShortData();
            objects[i].Object_Type = data.ObjectsDatabase[i].Object_Type;
            objects[i].Damage = data.ObjectsDatabase[i].Damage;
            objects[i].Health = data.ObjectsDatabase[i].Health;

            switch (objects[i].Object_Type)
            {
                case ObjectType.berry:
                    objects[i].ExtraBallType = data.ObjectsDatabase[i].ExtraBallType;
                    objects[i].ExtraBallDuration = data.ObjectsDatabase[i].ExtraBallDuration;
                    break;
                case ObjectType.groot:
                    objects[i].HealthRegenAmount = data.ObjectsDatabase[i].HealthRegenAmount;
                    break;
                case ObjectType.stoneCube:
                    break;
                case ObjectType.bushCube:
                    break;
                case ObjectType.bomb:
                    objects[i].ExplosionRadius = data.ObjectsDatabase[i].ExplosionRadius;
                    objects[i].ExplosionDamage = data.ObjectsDatabase[i].ExplosionDamage;
                    break;
                case ObjectType.knight:
                    objects[i].FrontShield = data.ObjectsDatabase[i].FrontShield;
                    objects[i].BackShield = data.ObjectsDatabase[i].BackShield;
                    objects[i].RightShield = data.ObjectsDatabase[i].RightShield;
                    objects[i].LeftShield = data.ObjectsDatabase[i].LeftShield;
                    break;
                case ObjectType.ram:
                    break;
                case ObjectType.jelly:
                    objects[i].FragmentsCount = data.ObjectsDatabase[i].FragmentsCount;
                    break;
                case ObjectType.gunner:
                    objects[i].ShotDamage = data.ObjectsDatabase[i].ShotDamage;
                    objects[i].ShootDistance = data.ObjectsDatabase[i].ShootDistance;
                    break;
                default:
                    break;
            }
        }
        balance.ObjectsDatabase = objects;
        BallShortData[] balls = new BallShortData[data.BallsDatabase.Length];
        for (int i = 0; i < data.BallsDatabase.Length; i++)
        {
            balls[i] = new BallShortData();
            balls[i].Ball_Type = data.BallsDatabase[i].Ball_Type;
            balls[i].Speed = data.BallsDatabase[i].Speed;
            balls[i].Damage = data.BallsDatabase[i].Damage;
        }
        balance.BallsDatabase = balls;
        return balance;
    }

    private void ClearGroundList()
    {
        if (groundElements.Count > 0)
        {
            for (var i = groundElements.Count - 1; i >= 0; i--)
            {
                if (groundElements[i] == null)
                    groundElements.RemoveAt(i);
            }
        }
    }
}
