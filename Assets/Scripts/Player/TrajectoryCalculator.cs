﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryCalculator : MonoBehaviour
{
    [SerializeField]
    private Transform m_startPoint;
    [SerializeField]
    private Transform m_startPoint_left;
    [SerializeField]
    private Transform m_startPoint_right;
    [SerializeField]
    private LineRenderer m_secondLine;


    [SerializeField]
    private float m_maxRayDistance = 10f;

    [SerializeField]
    private int m_reflectCount = 5;
    [SerializeField]
    private LayerMask m_LayerMask;

    private int currentReflectCount;
    private LineRenderer myline;
    private List<Vector3> rayCollisions = new List<Vector3>();
    private Vector3 currentStartPos;
    private Vector3 currentStartPos_left;
    private Vector3 currentStartPos_right;
    private Vector3 currentDirection;
    // Use this for initialization
    void Start()
    {
        myline = GetComponent<LineRenderer>();
        myline.enabled = false;
    }


    public void Raycast()
    {
        myline.enabled = true;
        RaycastHit hit;
        rayCollisions.Clear();
        currentReflectCount = m_reflectCount;
        currentStartPos = m_startPoint.position;
        currentStartPos.y = 0.2f;
        currentDirection = transform.forward;
        Vector3 normal = Vector3.Cross(currentDirection, Vector3.up);
        currentStartPos_left = currentStartPos + (normal.normalized * 0.2f);
        currentStartPos_right = currentStartPos + (-normal.normalized * 0.2f);

        if (Physics.Raycast(currentStartPos, currentDirection, out hit, m_maxRayDistance))
        {
            Vector3 startPos = hit.point;
            rayCollisions.Add(startPos);
        }

        while (currentReflectCount > 0)
        {
            float distance = float.MaxValue;
            if (Physics.Raycast(currentStartPos_left, currentDirection, out hit, m_maxRayDistance, m_LayerMask, QueryTriggerInteraction.Ignore))
            {
                var mid = (hit.point - currentStartPos_left);
                distance = mid.magnitude;
                Debug.DrawLine(currentStartPos_left, hit.point, Color.red, 0.1f);
            }
            if (Physics.Raycast(currentStartPos_right, currentDirection, out hit, m_maxRayDistance, m_LayerMask, QueryTriggerInteraction.Ignore))
            {
                var mid = (hit.point - currentStartPos_right).magnitude;
                if (mid < distance)
                    distance = mid;
                Debug.DrawLine(currentStartPos_right, hit.point, Color.green, 0.1f);
            }
            if (Physics.Raycast(currentStartPos, currentDirection, out hit, m_maxRayDistance, m_LayerMask, QueryTriggerInteraction.Ignore))
            {
                Vector3 mid = hit.point - currentStartPos;
                if (mid.magnitude < distance)
                    distance = mid.magnitude;

                distance = currentReflectCount == 1 ?Mathf.Clamp(distance, 0, 2f) : distance;

                Vector3 hitPoint = Vector3.MoveTowards(currentStartPos, hit.point, distance);
                Debug.DrawRay(currentStartPos, mid.normalized * distance, Color.black, 0.1f);


                hitPoint.y = 0.2f;
                rayCollisions.Add(hitPoint);
                currentReflectCount--;
                currentDirection = Vector3.Reflect((hitPoint - currentStartPos), hit.normal).normalized;
                currentStartPos = hitPoint;

                normal = Vector3.Cross(currentDirection, Vector3.up);
                currentStartPos_left = currentStartPos + (normal.normalized * 0.2f);
                currentStartPos_right = currentStartPos + (-normal.normalized * 0.2f);
            }
            else
            {
                break;
            }
        }
        BuildLine();
    }

    private void BuildLine()
    {
        if (rayCollisions.Count >= 2)
            myline.positionCount = 2;
        else myline.positionCount = rayCollisions.Count;
        for (int i = 0; i < 2; i++)
        {
            Vector3 pos = rayCollisions[i];
            pos.y = 0.2f;
            myline.SetPosition(i, pos);
        }
        m_secondLine.positionCount = rayCollisions.Count - 2 > 0 ? rayCollisions.Count - 1 : 0;
        if (m_secondLine.positionCount > 0)
        {
            m_secondLine.enabled = true;
            for (int j = 1; j < rayCollisions.Count; j++)
            {
                Vector3 pos = rayCollisions[j];
                pos.y = 0.2f;
                m_secondLine.SetPosition(j-1, pos);
            }
        }
    }

    public void TurnOffLine()
    {
        myline.enabled = false;
        m_secondLine.enabled = false;
    }
}
