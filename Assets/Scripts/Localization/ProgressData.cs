﻿[System.Serializable]
public class ProgressData
{
    public ProgressDataItem[] savedData;
}

[System.Serializable]
public class ProgressDataItem
{
    public string key;
    public int value;
}
