﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalizationController : MonoBehaviour
{
    public static Dictionary<string, string> LocalizedText;

    public static Action<string> ChangeLocalization = (loc) =>
    {
        LoadLocalizationFile(loc);
        PlayerPrefs.SetString("Language", loc);
    };

    private void Start()
    {
        ChangeLocalization.Invoke(PlayerPrefs.GetString("Language", "ENG"));
    }

    public void SetLocalization(string localization)
    {
        ChangeLocalization.Invoke(localization);
    }

    private static void LoadLocalizationFile(string localization)
    {
        LocalizedText = new Dictionary<string, string>();

        string name = "Localization_" + localization;

        TextAsset asset = Resources.Load<TextAsset>(name);
        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(asset.ToString());

        for (int i = 0; i < loadedData.items.Length; i++)
        {
            LocalizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
        }


    }
}
