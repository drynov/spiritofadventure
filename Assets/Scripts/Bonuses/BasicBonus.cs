﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasicBonus : MonoBehaviour
{
    [SerializeField]
    protected BonusType type;
    public abstract void ActivateBonus();
    public abstract void DeactivateBonus();
    public abstract void AddBonus();
    public abstract void SpendBonus();
}
