﻿using UnityEngine;
using GameAnalyticsSDK;
using System;

public class CustomAnalytics : MonoBehaviour
{
    public static CustomAnalytics Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
            Destroy(this);
        else
            Instance = this;
        DontDestroyOnLoad(this);
    }

    public void SpendCoins(int price, string itemName, int itemsCount)
    {
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "Coin", price, "Money", "SpentCoins");
        if (itemName == "FireBall")
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, itemName, itemsCount, "Bonus", "1FireBall");
        if (itemName == "FireWall")
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, itemName, itemsCount, "Bonus", "1FireWall");
    }

    public void AddCoins(int amount)
    {
        GameAnalytics.NewResourceEvent(GAResourceFlowType.Source, "Coin", amount, "Money", "SpentCoins");
    }

    private void UpdateMoney(int obj)
    {
        if (obj < 0)
        {
            GameAnalytics.NewResourceEvent(GAResourceFlowType.Sink, "Coin", obj, "Money", "SpentCoins");
        }

    }

    public void StartTutorial()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Tutorial");
    }

    public void CompleteTutorial()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Tutorial");
    }

    public void FailedTutorial()
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Tutorial");
    }

    public void FailedLevel()
    {
        string level = "level" + PlayerPrefs.GetString("CurrentLevel").Remove(0, 7);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, level);
    }

    public void CompleteLevel()
    {
        string level = "level" + PlayerPrefs.GetString("CurrentLevel").Remove(0, 7);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, level);
    }

    public void StartLevel()
    {
        string level = "level" + PlayerPrefs.GetString("CurrentLevel").Remove(0, 7);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, level);
    }
}
