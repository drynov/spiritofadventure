﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PoolObject
{
    public GameObject target;
    public List<GameObject> PoolObjects = new List<GameObject>();

    public PoolObject(GameObject target, GameObject firstObject)
    {
        this.target = target;
        PoolObjects.Add(firstObject);
    }
}

public class ObjectsPool : CreateSingletonGameObject<ObjectsPool>
{
    private List<PoolObject> poolObjects = new List<PoolObject>();

    public GameObject GetObject (GameObject target)
    {
        return GetPoolObject(target);
    }
    
    private GameObject GetPoolObject(GameObject target)
    {
        GameObject obj = null;
        PoolObject pool = poolObjects.FirstOrDefault(item => item.target == target);       
        if(pool == null)
        {
            obj = Instantiate(target);
            poolObjects.Add(new PoolObject(target, obj));
            return obj;
        }

        obj = pool.PoolObjects.FirstOrDefault(item => item && !item.activeSelf);
        
        if(!obj)
        {
            obj = Instantiate(target);
            pool.PoolObjects.Add(obj);
        }
        obj.SetActive(true);
        return obj;
    }
}
