﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallChargeButton : MonoBehaviour
{
    [SerializeField]
    private Sprite m_baseIcon;

    private bool isActivated;

    public bool IsActivated
    {
        get { return isActivated; }
        set
        {
            isActivated = value;
            if(!isActivated)
            {
                Icon.sprite = m_baseIcon;
            }
        }
    }

    [HideInInspector]
    public Image Icon;

    // Use this for initialization
    void Awake()
    {
        Icon = GetComponent<Image>();
    }
}
