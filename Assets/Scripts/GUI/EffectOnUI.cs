﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectOnUI : MonoBehaviour
{
    [SerializeField]
    private Transform m_EffectPoint;
    [SerializeField]
    private GameObject m_VictoryEffect;

    // Use this for initialization
    void Awake()
    {
        Instantiate(m_VictoryEffect, m_EffectPoint);
    }
}
