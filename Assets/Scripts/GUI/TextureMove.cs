﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureMove : MonoBehaviour
{

    [SerializeField]
    private Material m_mat;
    [SerializeField]
    private int m_speed;

    private Vector2 offset;

    // Use this for initialization
    void Start()
    {
        offset = m_mat.mainTextureOffset;

    }

    // Update is called once per frame
    void Update()
    {
        offset.x += Time.deltaTime * m_speed;
        m_mat.mainTextureOffset = offset;
    }
}
