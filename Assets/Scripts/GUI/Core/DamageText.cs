﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageText : MonoBehaviour
{
    public enum StyleScroll { none, ScrollUp, ScrollDown }

    public StyleScroll styleScroll = StyleScroll.none;

    [SerializeField]
    private Text m_Text;

    private Animator anim;
    private Color startColor;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        startColor = m_Text.color;
    }

    private void LateUpdate()
    {
        m_Text.color = startColor;
    }

    public void ShowText(string text, StyleScroll type = StyleScroll.ScrollUp, Color color = default(Color))
    {
        if (color != default(Color))
        {
            m_Text.color = color;
            startColor = color;
        }

        m_Text.text = text;
        if (styleScroll != StyleScroll.none)
        {
            anim.SetTrigger(styleScroll.ToString());
        }
        else
        {
            anim.SetTrigger(type.ToString());
        }
        Destroy(gameObject, 0.5f);
    }
}
