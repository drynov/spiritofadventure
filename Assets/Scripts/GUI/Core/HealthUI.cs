﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    public static HealthUI Instance;

    [SerializeField]
    private DamageText m_DamageText;
    [SerializeField]
    private Slider m_PlayerHPSlider;
    [SerializeField]
    private Text m_PlayerHPText;

    private Camera mainCam;

    private void Awake()
    {
        if (!Instance)
            Destroy(Instance);
        Instance = this;
    }

    private void Start()
    {
        mainCam = SceneManagerScript.Instance.m_mainCamera;
    }

    public void SetPlayerHP(int value, int maxValue, int damage)
    {
        m_PlayerHPSlider.maxValue = maxValue;
        value = value >= 0 ? value : 0;
        m_PlayerHPSlider.value = value;
        m_PlayerHPText.text = value.ToString();
        //TODO Animate damage Player 
        if (damage < 0)
        {
            var dt = Instantiate(m_DamageText, transform);
            dt.ShowText(damage.ToString(), DamageText.StyleScroll.ScrollDown);
        }
    }

    public void ShowDamage(string damage, Transform target)
    {
        Vector3 position = mainCam.WorldToScreenPoint(target.position);
        var dt = Instantiate(m_DamageText, position, Quaternion.identity, transform);
        dt.ShowText(damage);
    }

    public void ShowHeal(string damage, Transform target)
    {
        Vector3 position = mainCam.WorldToScreenPoint(target.position);
        var dt = Instantiate(m_DamageText, position, Quaternion.identity, transform);
        dt.ShowText(damage, DamageText.StyleScroll.ScrollUp, Color.green);
    }
}
