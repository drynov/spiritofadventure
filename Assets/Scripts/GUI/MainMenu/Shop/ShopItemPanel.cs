﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopItemPanel : MonoBehaviour
{
    [SerializeField]
    private Text m_Header;
    [SerializeField]
    private Text m_HeaderHalf;
    [SerializeField]
    private Text m_HeaderShadow;
    [SerializeField]
    private Text m_Description;
    [SerializeField]
    private Button m_ButtonPrice;
    [SerializeField]
    private Text m_CountText;
    [SerializeField]
    private Text m_CostText;
    [SerializeField]
    private Image m_Icon;

    private void Start()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void SetPrice(decimal price)
    {
        //m_ButtonPrice.GetComponentInChildren<Text>().text = @"Buy $ "+price.ToString();
        m_CostText.text = price.ToString() + " COINS";
    }
    public void SetName(string name)
    {
        List<char> separetedName = new List<char>();
        for (int j = 0; j < name.Length; j++)
        {
            if (Char.IsUpper(name[j]) && j != 0)
            {
                separetedName.Add(' ');
            }
            separetedName.Add(name[j]);
        }
        string newName = null;
        foreach (var c in separetedName)
        {
            newName += c;
        }
        newName = newName.ToUpper();
        m_Header.text =
        m_HeaderShadow.text =
        m_HeaderHalf.text = newName;
    }
    public void SetCount(int count)
    {
        m_CountText.text = "x" + count.ToString();
    }
    public void SetAction(UnityAction action)
    {
        m_ButtonPrice.onClick.AddListener(action);
    }

    public void SetIcon(Sprite icon)
    {
        m_Icon.sprite = icon;
    }

    public void FillItem(ShopItem item)
    {
        SetAction(item.Action);
        SetName(item.Name);
        SetPrice(item.Price);
        SetCount(item.Count);
        SetIcon(item.Icon);
    }
    private void OnDestroy()
    {
        m_ButtonPrice.onClick.RemoveAllListeners();
    }
}
