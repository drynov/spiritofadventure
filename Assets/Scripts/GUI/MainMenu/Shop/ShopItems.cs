﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct ShopItem
{
    public string Name;
    public int Count;
    public int Price;
    public UnityAction Action;
    public Sprite Icon; 
    public string Description;
}

public class ShopItems : MonoBehaviour
{
    [SerializeField]
    private ShopItemPanel m_ShopItemPrefab;
    [SerializeField]
    private int m_DeltaPositionItem;

    public List<ShopItemPanel> Items { get { return _Items; } }
    private List<ShopItemPanel> _Items = new List<ShopItemPanel>();
    private RectTransform _RectTransform;
    private RectTransform rectTransform { get { if (_RectTransform == null) _RectTransform = GetComponent<RectTransform>(); return _RectTransform; } }
    
    public void SetItems(ShopItem[] items)
    {
        //float sizeContent = 2160 + m_DeltaPositionItem + items.Length - 1;
        //rectTransform.offsetMin = new Vector2(-0.1f, -0.1f); // new Vector2(left, bottom);
        //rectTransform.offsetMax = new Vector2(sizeContent, -0.1f); // new Vector2(-right, -top);
        //float positionInParent = (2160 - sizeContent) / 2;
        m_DeltaPositionItem = (int)rectTransform.rect.width;
        print(m_DeltaPositionItem);
        float positionInParent = rectTransform.anchoredPosition.x;
        foreach (var item in items)
        {
            ShopItemPanel go = Instantiate(m_ShopItemPrefab, transform);
            go.transform.localPosition = new Vector3(positionInParent, 0, 0);
            _Items.Add(go);
            go.FillItem(item);
            positionInParent += m_DeltaPositionItem;
        }
    }
}
