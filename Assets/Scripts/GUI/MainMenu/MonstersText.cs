﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonstersText : MonoBehaviour
{
    private Text myText;
    private int totalEnemiesCount;
    private int currentSpawnedCount = 0;

    private void Awake()
    {
        myText = GetComponent<Text>();
    }
    // Use this for initialization
    void Start()
    {
        SceneManagerScript.Instance.OnLevelBuilt += UpdateMaxNumber;
        SceneObjectsManager.Instance.OnEnemySpawnAction += UpdateEnemiesCount;
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.OnLevelBuilt -= UpdateMaxNumber;
        SceneObjectsManager.Instance.OnEnemySpawnAction -= UpdateEnemiesCount;
    }

    private void UpdateMaxNumber()
    {
        myText.text = "0/" + LevelRoundLoader.Instance.TotalEnemiesCount.ToString();
    }

    private void UpdateEnemiesCount()
    {
        currentSpawnedCount++;
        myText.text = currentSpawnedCount + "/" + LevelRoundLoader.Instance.TotalEnemiesCount.ToString();
    }
}
