﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelsRoad : MonoBehaviour
{
    [SerializeField]
    private GameObject m_LevelMap;

    [SerializeField]
    private GameObject m_Content;


    // Use this for initialization
    void Start()
    {
        // массив данных о пройденых уровнях
        List<string> userLevelsData = new List<string>();
        List<string> userStarsData = new List<string>();
        // количество уровней в игре
        TextAsset[] assets = Resources.LoadAll<TextAsset>("Levels/");
        if (assets != null)
        {
            string levelsEncode = PlayerPrefs.GetString("AcessLevels");
            string levelStarsEncode = PlayerPrefs.GetString("LevelsStars");
            //дешифровка строки из настроек
            string levels = B64X.DecodeToString(levelsEncode);
            string levelStars = B64X.DecodeToString(levelStarsEncode);
            // дешифрование не вернуло нустую строку
            if (!String.IsNullOrEmpty(levels))
                userLevelsData = levels.Split(',').ToList();
            if (!String.IsNullOrEmpty(levelStars))
                userStarsData = levelStars.Split(',').ToList();
            if (userLevelsData.Count < assets.Length)
            {
                var length = assets.Length;
                //заполняем недостающие уровни
                for (int i = userLevelsData.Count; i < length; i++) // i = 3 ; 3 < 10 ; i++
                {
                    userLevelsData.Add("0");
                }
            }

            if (userStarsData.Count < assets.Length)
            {
                var length = assets.Length;
                //заполняем недостающие уровни
                for (int i = userStarsData.Count; i < length; i++) // i = 3 ; 3 < 10 ; i++
                {
                    userStarsData.Add("0");
                }
            }
            //в тукущий уровень проставляем победу
            userLevelsData[0] = "1";
            if (String.IsNullOrEmpty(userStarsData[0]))
                userStarsData[0] = "0";
        }
        else
        {
            Debug.LogError("Levels not found!");
            return;
        }

        var lengthUserData = userLevelsData.Count;
        int levelsCount = m_LevelMap.GetComponent<LevelsMap>().m_Levels.Length;
        int rest = lengthUserData % levelsCount;
        float mapsCount = lengthUserData / levelsCount;
        if (rest > 0)
            mapsCount++;

        int k = 0;
        for (int j = 0; j < mapsCount; j++)
        {
            var map = Instantiate(m_LevelMap);
            map.transform.SetParent(m_Content.transform);
            LevelsMapPoint[] mapLevels = map.GetComponent<LevelsMap>().m_Levels;

            for (int i = j == 0 ? 1 : 0; i < levelsCount; i++)
            {
                int stars = 0;
                bool isUnlocked = false;
                if (k < userLevelsData.Count)
                {
                    bool success = int.TryParse(userStarsData[k], out stars);
                    isUnlocked = userLevelsData[k] == "1";
                }
                var index = k + 1;
                mapLevels[i].SetPoint(delegate { LoadLevel(index); }, isUnlocked, stars, index);
                k++;
            }
        }
    }

    public void LoadLevel(int level)
    {
        string path = "Levels/" + level.ToString();
        if (Resources.Load<TextAsset>(path) == null)
        {
            Debug.LogError("Wrong name!");
            return;
        }
        else
        {
            PlayerPrefs.SetString("CurrentLevel", path);
        }
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
    }
}
