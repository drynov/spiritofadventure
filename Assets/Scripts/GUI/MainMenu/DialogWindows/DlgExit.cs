﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DlgExit : DlgAbstract
{

	public void ExitYes()
    {
        Application.Quit();
    }
}
