﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class DlgVictory : DlgAbstract
{
    [SerializeField]
    private Image m_StarsImage;
    [SerializeField]
    private Sprite m_Stars_0;
    [SerializeField]
    private Sprite m_Stars_1;
    [SerializeField]
    private Sprite m_Stars_2;
    [SerializeField]
    private Sprite m_Stars_3;
    [SerializeField]
    private Camera m_EffectUICamera;
    [SerializeField]
    private Text m_MoneyText;

    private GameUI main;

    protected new void Awake()
    {
        base.Awake();
        m_StarsImage.sprite = m_Stars_0;
        main = transform.root.GetComponent<GameUI>();
        Instantiate(m_EffectUICamera);
        MarketUI.Instance.CountScore(m_MoneyText);
        //MarketUI.coinsText = m_MoneyText;
    }

    private void OnEnable()
    {
        //ADsManager.Instance.ViewAds();
        //int nm = 1;
        //Analytics.CustomEvent("LevelVictory", new Dictionary<string, object>
        //{
        //    {"level_complete", nm }
        //});
    }

    public void MainMenu()
    {
        main.LoadMainMenu();
    }
    public void Replay()
    {
        main.ReloadLevel();
    }
    public void LoadNextLevel()
    {
        string name = PlayerPrefs.GetString("CurrentLevel", "1");
        name = name.Remove(0, 7);
        int i = int.Parse(name);
        i++;
        name = "Levels/" + i.ToString();
        if (Resources.Load<TextAsset>(name) == null)
        {
            Debug.LogError("Wrong name!");
            return;
        }
        else
        {
            PlayerPrefs.SetString("CurrentLevel", name);
        }
        SceneLoader.LoadLevel(new NextLevelData("SampleScene", true));
        //main.LoadNextLevel();
    }

    public void SetStars(int count)
    {
        if (count == 0)
        {
            m_StarsImage.sprite = m_Stars_0;
        }
        else if (count == 1)
        {
            m_StarsImage.sprite = m_Stars_1;
        }
        else if (count == 2)
        {
            m_StarsImage.sprite = m_Stars_2;
        }
        else if (count == 3)
        {
            m_StarsImage.sprite = m_Stars_3;
        }
        else
        {
            Debug.LogError("Wrong stars count=" + count);
        }
    }
}
