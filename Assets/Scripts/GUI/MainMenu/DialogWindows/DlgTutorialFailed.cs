﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class DlgTutorialFailed : DlgAbstract
{
    private GameUI main;

    protected new void Awake()
    {
        base.Awake();
        main = transform.root.GetComponent<GameUI>();
    }

    private void OnEnable()
    {

    }

    public void ReloadLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Tutorial_SampleScene");
    }
}