﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    [SerializeField]
    private Text m_Text;
    [SerializeField]
    private Image m_LevelLockImage;

    private Button _button;

    public int Id
    {
        get;
        internal set;
    }

    private Button button
    {
        get
        {
            if (_button == null)
            {
                _button = GetComponent<Button>();
            }
            return _button;
        }
    }

    public void Set(UnityAction onClick, string text, bool interactable)
    {
        button.onClick.AddListener(onClick);
        m_Text.text = text;
        button.interactable = m_Text.enabled = interactable;
        m_LevelLockImage.enabled = !interactable;
    }
}
