﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MarketUI : MonoBehaviour
{
    public static MarketUI Instance { get; private set; }

    [SerializeField]
    private Text m_GoldText;
    [SerializeField]
    private Animator m_MoneyAnimator;

    //public static Text coinsText;
    private int currentGold;
    private int levelIncome = 0;
    private int coinsPerStar = 50;

    private IEnumerator myCoroutine;

    // Use this for initialization
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
        int value = ProgressDataController.Instance.GetData("CoinsBank");
        currentGold = value >= 0 ? value : 0;
        ShowMoney();

        SceneManagerScript.Instance.UpdateMoney += UpdateMoney;
        SceneManagerScript.Instance.OnLevelBuilt += SetCoinIncome;
        //SceneManagerScript.OnVictoryAction += CountScore;
    }

    private void SetCoinIncome()
    {
        coinsPerStar = Resources.Load<PlayerCharacterData>("ObjectsDatabases/PlayerData").CoinsPerStar;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            currentGold += 1000;
            ProgressDataController.Instance.SetData("CoinsBank", currentGold);
            ShowMoney();
        }
    }

    private void OnDestroy()
    {
        SceneManagerScript.Instance.UpdateMoney -= UpdateMoney;
        SceneManagerScript.Instance.OnLevelBuilt -= SetCoinIncome;
        //SceneManagerScript.OnVictoryAction -= CountScore;
    }

    private void UpdateMoney(int money)
    {
        if (money > 0)
        {
            AddMoney(money);
        }
        else
        {
            SpendMoney(Mathf.Abs(money));
        }
    }

    private void ShowMoney()
    {
        if (levelIncome > 0)
            m_GoldText.text = currentGold.ToString() + " + " + levelIncome.ToString();
        else
            m_GoldText.text = currentGold.ToString();
    }

    public void AddMoney(int amount)
    {
        if (SceneManager.GetActiveScene().name == "SampleScene")
            levelIncome += amount;
        else
            currentGold += amount;
        ShowMoney();
        m_MoneyAnimator.SetTrigger("Add");
    }

    public void SpendMoney(int amount)
    {
        if (amount <= currentGold)
        {
            currentGold -= amount;
            ShowMoney();
            ProgressDataController.Instance.SetData("CoinsBank", currentGold);
            m_MoneyAnimator.SetTrigger("Spend");
        }
    }

    private bool GooglePayCheck()
    {
        return true;
    }

    public void CountScore(Text text)
    {
        if (myCoroutine != null)
            return;

        myCoroutine = CountMoneyCoroutine(text);
        StartCoroutine(myCoroutine);
    }

    private IEnumerator CountMoneyCoroutine(Text coinsText)
    {
        levelIncome = (StarsController.Instance.TotalStars - StarsController.Instance.LastTotalStars) * coinsPerStar;

        int fakeMoney = 0;
        int fakeIncome = 0;
        int value = ProgressDataController.Instance.GetData("CoinsBank");
        currentGold = Mathf.Clamp(value, 0, value);
        fakeMoney = currentGold;
        fakeIncome = levelIncome;
        currentGold += levelIncome;
        levelIncome = 0;
        ProgressDataController.Instance.SetData("CoinsBank", currentGold);
        yield return new WaitForSeconds(0.6f);
        coinsText.gameObject.SetActive(true);
        //CustomAnalytics.Instance.AddCoins(fakeIncome);
        if (fakeIncome <= 0)
        {
            coinsText.text = fakeMoney.ToString();
        }
        else
        {
            while (fakeIncome > 0)
            {
                fakeMoney += 5;
                fakeIncome -= 5;
                coinsText.text = fakeMoney.ToString();
                //coinsText.text = fakeMoney.ToString() + " + " + fakeIncome.ToString();
                yield return new WaitForSeconds(0.05f);
            }
            coinsText.text = fakeMoney.ToString();
        }
        myCoroutine = null;
    }
}
