﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Tweener : CreateSingletonGameObject<Tweener>
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="target">Tрансформ который нужно двигать</param>
    /// <param name="from">откуда двигать</param>
    /// <param name="to">куда двигать</param>
    /// <param name="time">за какое время двигать</param>
    /// <param name="callbackComplete">ОПЦИОНАЛЬНО - метод для обратного вызова после завершения движения</param>
    /// <param name="curve">ОПЦИОНАЛЬНО - кривая движения</param>
    public static void TweenToPosition(Transform target, Vector3 from, Vector3 to, float time, Action callbackComplete = null, AnimationCurve curve = null)
    {
        Tweener_ObjectPosition.Instance.TweenToNewPosition(target, from, to, time, callbackComplete, curve);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj">Обьект который нужно двигат</param>
    /// <param name="from">откуда двигать</param>
    /// <param name="to">куда двигать</param>
    /// <param name="time">за какое время двигать</param>
    /// <param name="callbackComplete">ОПЦИОНАЛЬНО - метод для обратного вызова после завершения движения</param>
    /// <param name="curve">ОПЦИОНАЛЬНО - кривая движения</param>
    public static void TweenToPosition(GameObject obj, Vector3 from, Vector3 to, float time, Action callbackComplete = null, AnimationCurve curve = null)
    {
        Tweener_ObjectPosition.Instance.TweenToNewPosition(obj.transform, from, to, time, callbackComplete, curve);
    }



    public static void StopTweenToPosition(Transform target)
    {
        Tweener_ObjectPosition.Instance.StopTweenToNewPosition(target);
    }

    public static void StopTweenToPosition(GameObject obj)
    {
        Tweener_ObjectPosition.Instance.StopTweenToNewPosition(obj.transform);
    }

}
