﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Tweener_ObjectPosition : CreateSingletonGameObject<Tweener_ObjectPosition>
 {
    private Dictionary<Transform, Coroutine> tweenCoroutines = new Dictionary<Transform, Coroutine>();

    public void TweenToNewPosition(Transform target, Vector3 from, Vector3 to, float time, Action callbackComplete = null, AnimationCurve curve = null)
    {
        StopTweenToNewPosition(target);
        tweenCoroutines.Add(target, StartCoroutine(TweenToPositionCoroutine(target, from, to, time, callbackComplete, curve)));
    }

    public void StopTweenToNewPosition(Transform target)
    {
        Coroutine currentCoroutine = null;
        tweenCoroutines.TryGetValue(target, out currentCoroutine);

        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
            tweenCoroutines.Remove(target);
        }
    }

    private IEnumerator TweenToPositionCoroutine(Transform target, Vector3 from, Vector3 to, float time, Action callbackComplete = null, AnimationCurve curve = null)
    {
        float k = 0f;
        while (k < 1f)
        {
            float t = curve == null ? k : curve.Evaluate(k);
            target.position = Vector3.Lerp(from, to, t);
            k += Time.deltaTime / time;
            
            yield return null;
        }

        if (callbackComplete != null)
        {
            callbackComplete.Invoke();
        }

        target.position = to;

        StopTweenToNewPosition(target);

        if(tweenCoroutines.Count == 0)
        {
            Destroy(gameObject);
        }
    }
}
