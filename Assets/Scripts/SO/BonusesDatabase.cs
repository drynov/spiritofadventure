﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "BonusesDatabase", menuName = "Data/BonusData", order = 1)]
public class BonusesDatabase : ScriptableObject
{
    public BonusData[] BonusDatabase;

    public BonusData GetObjectByID(int ID)
    {
        for (int i = 0; i < BonusDatabase.Length; i++)
        {
            if (BonusDatabase[i].Bonus_ID == ID)
            {
                return BonusDatabase[i];
            }
        }
        return null;
    }

    public BonusData GetObjectByType(BonusType type)
    {
        for (int i = 0; i < BonusDatabase.Length; i++)
        {
            if (BonusDatabase[i].Bonus_Type == type)
            {
                return BonusDatabase[i];
            }
        }
        return null;
    }

}

public enum BonusType { FireWall, FireBall}

[Serializable]
public class BonusData
{
    public int Bonus_ID;
    public BonusType Bonus_Type;
    public GameObject Bonus_Prefab;
    public AudioClip Bonus_Audio;
    public Sprite Bonus_Icon;
    public Sprite Bonus_ActivationIcon;
    public int Bonus_Cooldown;

    public BonusData(int ID, BonusType type, GameObject prefab, AudioClip audio, Sprite icon, Sprite activeIcon,int cooldown)
    {
        Bonus_ID = ID;
        Bonus_Type = type;
        Bonus_Prefab = prefab;
        Bonus_Audio = audio;
        Bonus_Icon = icon;
        Bonus_ActivationIcon = activeIcon;
        Bonus_Cooldown = cooldown;
    }
}



