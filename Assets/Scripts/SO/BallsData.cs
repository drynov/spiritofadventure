﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "BallsDatabase", menuName = "Data/BallsData", order = 1)]
public class BallsData : ScriptableObject
{
    public BallInfo[] BallsDatabase;

    public BallInfo GetBalllByID(int ID)
    {
        for (int i = 0; i < BallsDatabase.Length; i++)
        {
            if (BallsDatabase[i].Ball_ID == ID)
            {
                return BallsDatabase[i];
            }
        }
        return null;
    }

    public BallInfo GetBallByType(BallType type)
    {
        for (int i = 0; i < BallsDatabase.Length; i++)
        {
            if (BallsDatabase[i].Ball_Type == type)
            {
                return BallsDatabase[i];
            }
        }
        return null;
    }

}

[Serializable]
public class BallInfo
{
    public int Ball_ID;
    public BallType Ball_Type;
    public Sprite Ball_Icon;
    public GameObject Ball_Prefab;
    public float Ball_speed;
    public int Ball_damage;

    public BallInfo(BallType type, GameObject prefab, Sprite icon, float speed, int damage)
    {
        Ball_Type = type;
        Ball_Icon = icon;
        Ball_Prefab = prefab;
        Ball_speed = speed;
        Ball_damage = damage;
    }
}

