﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelBalanceData", menuName = "Data/LevelBalanceData", order = 1)]
[System.Serializable]
public class LevelBalanceData : ScriptableObject
{
    [Header("Scene objects settings")]
    public ObjectShortData[] ObjectsDatabase;
    [Header("Balls settings")]
    public BallShortData[] BallsDatabase;

    [Header("Global level settings")]
    [Range(1,50)]
    public int BallsPerTurn;

    [Range(0.1f, 1f)]
    public float BetweenBallsTimer;

    [Range(1, 20)]
    public int PlayerLifes;

    [Range(0, 1000)]
    public int CoinsInChest;
}