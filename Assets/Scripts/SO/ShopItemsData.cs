﻿using UnityEngine;

[CreateAssetMenu(fileName = "ShopItemsDatabase", menuName = "Data/ShopData", order = 1)]
public class ShopItemsData : ScriptableObject
{
    public ShopItem[] ShopDatabase;

    public ShopItem GetObjectByName(string name)
    {
        for (int i = 0; i < ShopDatabase.Length; i++)
        {
            if (ShopDatabase[i].Name == name)
            {
                return ShopDatabase[i];
            }
        }
        return new ShopItem();
    }
}