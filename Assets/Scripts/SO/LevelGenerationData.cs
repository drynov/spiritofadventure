﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "LevelGenerationData", menuName = "Data/LevelGenerationData", order = 1)]
public class LevelGenerationData : ScriptableObject
{
    public GameObject[] FloorElement;
    public int FloorElementSizeX;
    public int FloorElementSizeZ;
    public GameObject LeftBorderElement;
    public GameObject RightBorderElement;
    public GameObject FrontBorderElement;
    public GameObject FrontRightBorderElement;
    public GameObject FrontLeftBorderElement;
    public GameObject Gates;
    public GameObject Collider;
    public GameObject FloorCollider;
}
