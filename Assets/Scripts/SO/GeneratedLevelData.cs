﻿using UnityEngine;

[System.Serializable]
public class GeneratedLevelData
{
    public BalanceData BalanceData;
    public GeneratedLevelItem LevelRoot;
    public GeneratedLevelItem LevelBase;
    public GeneratedRound[] Rounds;
    public int StartWavesCount;
}

[System.Serializable]
public class GeneratedRound
{
    public GeneratedLevelItem[] RoundItems;
}

[System.Serializable]
public class GeneratedLevelItem
{
    public string Marker;
    public string Name;
    public string Tag;
    public Vector3 Position;
    public Quaternion Rotation;
    public Vector3 Scale;
    //public Vector3 colliderSize;
    //public int Health;
}