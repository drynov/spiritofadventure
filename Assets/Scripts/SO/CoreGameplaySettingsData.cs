﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CoreGameplaySettingsData", menuName = "Data/GameplaySettings", order = 1)]
public class CoreGameplaySettingsData : ScriptableObject
{
    public int TurnsPerLevel;
}
