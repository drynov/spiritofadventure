﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum TutorialTarget
{
    undefined,
    sceneObject,
    button,
    rect,
    textMessage
}

public interface ITutorialElement
{
    int Number { get;}
    Action<bool> StopAction { get; set; }
    void StartTutorial(object obj, TutorialTarget target);
    void StopTutorial(bool isBreak);
}
