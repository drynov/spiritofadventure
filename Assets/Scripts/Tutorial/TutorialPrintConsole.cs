﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPrintConsole : MonoBehaviour, ITutorialElement
{
    public int Number
    {
        get
        {
            return m_Number;
        }
    }

    public Action<bool> StopAction
    {
        get; set;
    }

    [SerializeField]
    private int m_Number;

    public void StartTutorial(object obj, TutorialTarget target)
    {
        if(target != TutorialTarget.undefined)
        {
            StopTutorial(false);
            return;
        }

        if(obj.GetType() != typeof(GameObject))
        {
            StopTutorial(false);
            return;
        }

        StartCoroutine(Test((GameObject)obj));
    }

    public void StopTutorial(bool isBreak)
    {
        if(StopAction != null)
        {
            StopAction.Invoke(isBreak);
        }
    }

    private IEnumerator Test(GameObject obj)
    {
        print("StartTutorial " + obj.name);
        yield return new WaitForSeconds(1f);
        print("StopTutorial " + obj.name);
        yield return new WaitForSeconds(1f);
        StopTutorial(false);
    }

}
