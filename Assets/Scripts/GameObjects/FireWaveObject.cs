﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWaveObject : MonoBehaviour
{
    private int mySpeed = 0;
    private int myDamage = 0;

    public void SetParameters(int speed, int damage)
    {
        mySpeed = speed;
        myDamage = damage;
    }

    private void OnTriggerEnter(Collider other)
    {
        Health enemy = other.gameObject.GetComponent<Health>();
        if (enemy)
        {
            enemy.TakeDamage(gameObject, myDamage);
        }
    }

    private void Update()
    {
        transform.Translate(transform.forward * Time.deltaTime * mySpeed);
    }
}
