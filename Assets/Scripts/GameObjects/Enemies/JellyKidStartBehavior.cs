﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyKidStartBehavior : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private GameObject graphics;

    [SerializeField]
    private GameObject grownGraphics;

    private EnemyStateMachine myStates;
    private Rigidbody myRigidbody;
    private JellyKidHealth myHealth;
    private Collider myCollider;
    private int startHealth;
    private IEnumerator myCoroutine;

    private void Awake()
    {
        myCollider = GetComponent<Collider>();
        myHealth = GetComponent<JellyKidHealth>();
        myRigidbody = GetComponent<Rigidbody>();
        myStates = GetComponent<EnemyStateMachine>();
        GameStateMachine.Instance.OnEnemyTurnAction += ActivateObject;
    }

    private void Start()
    {
        myStates.IsReady = true;
    }

    private void OnDestroy()
    {
        GameStateMachine.Instance.OnEnemyTurnAction -= ActivateObject;
    }

    public void SetData(Vector3 appearPos, int hp)
    {
        startHealth = hp;
        appearPos.y = 0;
        MoveToStartPosition(appearPos);
    }

    public void MoveToStartPosition(Vector3 pos)
    {
        if (myCoroutine != null)
            return;

        if ((transform.position - pos).sqrMagnitude > 0.1f)
        {
            myCoroutine = MoveCoroutine(pos);
            StartCoroutine(myCoroutine);
        }
        else
            transform.position = pos;
    }

    private IEnumerator MoveCoroutine(Vector3 pos)
    {
        Vector3 startPos = transform.position;
        startPos.y = 0;
        Vector3 direction = startPos - pos;
        speed = speed * direction.magnitude;
        print(speed);
        while (true)
        {
            transform.Translate(direction.normalized * Time.deltaTime * speed);
            Vector3 groundPos = transform.position;
            groundPos.y = 0;
            if ((pos - groundPos).sqrMagnitude <= 0.1f)
                break;
            yield return null;
        }
        transform.position = pos;
        myCoroutine = null;
    }

    private void ActivateObject()
    {
        StartCoroutine(ActivationCoroutine());
    }

    public void TurnOnJelly()
    {
        grownGraphics.SetActive(true);
        Destroy(graphics, Time.deltaTime);
        myCollider.enabled = true;
        myRigidbody.useGravity = true;
        myHealth.ActivateJelly(startHealth);
        Destroy(this, Time.deltaTime);
    }

    private IEnumerator ActivationCoroutine()
    {
        while (myCoroutine != null)
        {
            yield return null;
        }

        GetComponent<Animator>().SetTrigger("appear");
        myStates.IsReady = false;
        myStates.m_HaveAppearAnimation = false;
        myStates.MoveForward();
    }
}