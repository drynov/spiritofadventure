﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEffectsInitializer : MonoBehaviour
{
    [SerializeField]
    private GameObject m_StartEffect;
    [SerializeField]
    private float m_StartEffectTimer = 2f;
    [SerializeField]
    private bool m_IsPlaceToGround = true;

	// Use this for initialization
	void Start ()
    {
		if(m_StartEffect)
        {
            Vector3 pos = transform.position;
            if(m_IsPlaceToGround)
            {
                pos.y = 0f;
            }

            GameObject obj = Instantiate(m_StartEffect, pos, m_StartEffect.transform.rotation);
            Destroy(obj, m_StartEffectTimer);
        }
	}
	

}
