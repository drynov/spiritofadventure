﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightHealth : ObjectHealth
{
    [SerializeField]
    private GameObject[] m_Shields;
    
    public bool isFrontShield;
    public bool isBackShield;
    public bool isRightShield;
    public bool isLeftShield;
    

    private void Start()
    {
        GetData();
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        isFrontShield = data.GetObjectByType(m_ObjectType).Object_FrontShield;
        m_Shields[0].SetActive(isFrontShield);
        isBackShield = data.GetObjectByType(m_ObjectType).Object_BackShield;
        m_Shields[1].SetActive(isBackShield);
        isRightShield = data.GetObjectByType(m_ObjectType).Object_RightShield;
        m_Shields[2].SetActive(isRightShield);
        isLeftShield = data.GetObjectByType(m_ObjectType).Object_LeftShield;
        m_Shields[3].SetActive(isLeftShield);
    }

    public override void TakeDamage(GameObject damageDealer, int value)
    {
        var ball = damageDealer.GetComponent<BallBehavior>();
        if (ball)
        {
            if (isFrontShield)
            {
                if (damageDealer.transform.position.z < transform.position.z - 0.5f)
                    return;
            }
            if (isBackShield)
            {
                if (damageDealer.transform.position.z > transform.position.z + 0.5f)
                    return;
            }
            if (isLeftShield)
            {
                if (damageDealer.transform.position.x < transform.position.x - 0.5f)
                    return;
            }
            if (isRightShield)
            {
                if (damageDealer.transform.position.x > transform.position.x + 0.5f)
                    return;
            }
        }
        int currentHP = m_hp - value;
        m_hp = Mathf.Clamp(currentHP, 0, startHP);
        lifebar.UpdateLifeBar(m_hp, startHP, -value);

        if (m_hp <= 0)
        {
            Die();
            return;
        }

        if (m_Animator && value > 0)
        {
            if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("IdleTree"))
            {
                m_Animator.SetTrigger("hit");
            }
            if (hitVFX)
            {
                Instantiate(hitVFX, transform.position, Quaternion.identity);
            }
            if (hitAudioClip)
            {
                AudioSource.PlayClipAtPoint(hitAudioClip, Camera.main.transform.position);
            }
        }
    }
}
