﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrootRegeneration : MonoBehaviour
{
    private EnemyStateMachine myBehavior;
    private ObjectHealth myHealth;
    private int regenAmount;

    // Use this for initialization
    void Start()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        regenAmount = data.GetObjectByType(ObjectType.groot).Object_HealthRegen;
        myBehavior = GetComponent<EnemyStateMachine>();
        myHealth = GetComponent<ObjectHealth>();
        myBehavior.OnFinishMoveAction += RegenHealth;
    }

    private void OnDestroy()
    {
        myBehavior.OnFinishMoveAction -= RegenHealth;
    }

    private void RegenHealth()
    {
        myHealth.TakeHeal(gameObject, regenAmount);
    }
}
