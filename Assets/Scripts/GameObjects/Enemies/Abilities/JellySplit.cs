﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellySplit : MonoBehaviour
{
    [SerializeField]
    private GameObject m_jellyKid;

    [SerializeField]
    private int m_leftBorder;
    [SerializeField]
    private int m_rightBorder;
    [SerializeField]
    private int m_topBorder;
    [SerializeField]
    private int m_botBorder;

    private int partsCount = 2;
    private int firstStagePlaces;
    private int kidHealth;
    private ObjectHealth myHealth;
    private List<Vector3> PossiblePositions = new List<Vector3>();
    private GameObject kid;
    // Use this for initialization
    void Start()
    {
        GetData();
        myHealth.OnDeathAction += OnDeathSplit;
    }

    private void OnDestroy()
    {
        myHealth.OnDeathAction -= OnDeathSplit;
    }

    [ContextMenu("Debug Split")]
    public void Split()
    {
        PossiblePositions.Clear();
        OnDeathSplit();
    }

    private void OnDeathSplit()
    {
        FindFirstStagePositions();

        if (PossiblePositions.Count < partsCount)
        {
            FindSecondStagePositions();
        }

        SpawnJellyKids();
    }

    private void FindFirstStagePositions()
    {
        // find free positions on first cirlce of tiles
        List<Vector3> FirstStagePositions = new List<Vector3>();
        for (int i = -1; i <= 1; i++)
        {
            int newX = Mathf.RoundToInt(transform.position.x + i);
            if (newX < m_leftBorder || newX > m_rightBorder)
                continue;
            for (int j = -1; j <= 1; j++)
            {
                int newZ = Mathf.RoundToInt(transform.position.z + j);
                if (newZ < m_botBorder || newZ > m_topBorder)
                    continue;
                FirstStagePositions.Add(new Vector3(newX, 0.5f, newZ));
            }
        }

        for (int i = 0; i < FirstStagePositions.Count; i++)
        {
            if (Physics.OverlapSphere(FirstStagePositions[i], 0.1f).Length <= 0)
            {
                PossiblePositions.Add(FirstStagePositions[i]);
            }
        }

        firstStagePlaces = PossiblePositions.Count;
    }

    private void FindSecondStagePositions()
    {
        // find free positions on second cirlce of tiles
        List<Vector3> SecondStagePositions = new List<Vector3>();
        for (int i = -2; i <= 2; i+=4)
        {
            int newX = Mathf.RoundToInt(transform.position.x + i);
            if (newX < m_leftBorder || newX > m_rightBorder)
                continue;
            for (int j = -2; j <= 2; j++)
            {
                int newZ = Mathf.RoundToInt(transform.position.z + j);
                if (newZ < m_botBorder || newZ > m_topBorder)
                    continue;
                SecondStagePositions.Add(new Vector3(newX, 0.5f, newZ));
            }
        }

        for (int i = -1; i <= 1; i ++)
        {
            int newX = Mathf.RoundToInt(transform.position.x + i);
            if (newX < m_leftBorder || newX > m_rightBorder)
                continue;
            for (int j = -2; j <= 2; j+=4)
            {
                int newZ = Mathf.RoundToInt(transform.position.z + j);
                if (newZ < m_botBorder || newZ > m_topBorder)
                    continue;
                SecondStagePositions.Add(new Vector3(newX, 0.5f, newZ));
            }
        }

        for (int i = 0; i < SecondStagePositions.Count; i++)
        {
            if (Physics.OverlapSphere(SecondStagePositions[i], 0.1f).Length <= 0)
            {
                PossiblePositions.Add(SecondStagePositions[i]);
            }
        }
    }

    private void SpawnJellyKids()
    {
        if (PossiblePositions.Count <= 0)
            return;

        if(partsCount > PossiblePositions.Count)
        {
            foreach (var item in PossiblePositions)
            {
                SpawnOneKid(item);
            }
            return;
        }

        if (partsCount > firstStagePlaces)
        {
            for (int i = 0; i < firstStagePlaces; i++)
            {
                SpawnOneKid(PossiblePositions[i]);
            }
            PossiblePositions.RemoveRange(0, firstStagePlaces);
            for (int i = 0; i < partsCount-firstStagePlaces; i++)
            {
                int nm = Random.Range(0, PossiblePositions.Count);
                SpawnOneKid(PossiblePositions[nm]);
                PossiblePositions.RemoveAt(nm);
            }
        }
        else if (partsCount < firstStagePlaces)
        {
            for (int i = 0; i < partsCount; i++)
            {
                int nm = Random.Range(0, PossiblePositions.Count);
                SpawnOneKid(PossiblePositions[nm]);
                PossiblePositions.RemoveAt(nm);
            }
        }
        else
        {
            for (int i = 0; i < partsCount; i++)
            {
                SpawnOneKid(PossiblePositions[i]);
            }
        }
    }

    private void SpawnOneKid(Vector3 position)
    {
        GameObject jellyKid = Instantiate(m_jellyKid, transform.position, m_jellyKid.transform.rotation);
        jellyKid.GetComponent<JellyKidStartBehavior>().SetData(position, kidHealth);
        LevelRoundLoader.Instance.TotalEnemiesCount++;
    }

    private void GetData()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        data = TutorialLevel.SceneObjectsData(data);
        partsCount = data.GetObjectByType(ObjectType.jelly).Object_FragmentsCount;
        //m_jellyKid = data.GetObjectByType(ObjectType.jelly).Object_ReplacingObjectPrefab;
        myHealth = GetComponent<ObjectHealth>();
        kidHealth = Mathf.FloorToInt(myHealth.HP / partsCount);
        kidHealth = Mathf.Clamp(kidHealth, 1, myHealth.HP);
    }
}
