﻿using System;
using UnityEngine;

public class GunnerShot : MonoBehaviour
{
    [SerializeField]
    private GameObject m_bullet;

    [SerializeField]
    private Transform m_bulletStartPoint;

    private EnemyStateMachine myStates;
    private Animator myAnimator;

    private int bulletDamage;
    private float shootRange;


    // Use this for initialization
    void Start()
    {
        SceneObjectsData data = Resources.Load<SceneObjectsData>("ObjectsDatabases/SceneObjectsData");
        bulletDamage = data.GetObjectByType(ObjectType.gunner).Object_ShotDamage;
        shootRange = data.GetObjectByType(ObjectType.gunner).Object_ShootDistance++;
        myAnimator = GetComponent<Animator>();
        myStates = GetComponent<EnemyStateMachine>();
        myStates.OnFinishMoveAction += Shoot;
    }

    private void OnDestroy()
    {
        myStates.OnFinishMoveAction -= Shoot;
    }

    private void Shoot()
    {
        RaycastHit hit;
        Vector3 pos = transform.position;
        pos.y = 0.5f;
        if (Physics.Raycast(pos, transform.forward, out hit, shootRange))
        {
            Debug.DrawLine(pos, hit.point);
            BallDestroyer wall = hit.collider.GetComponent<BallDestroyer>();
            if (wall)
            {
                //myAnimator.SetTrigger("shoot");
                GunnerBulletShot();
            }
        }
    }

    public void GunnerBulletShot()
    {
        GameObject bullet = Instantiate(m_bullet, m_bulletStartPoint.position, m_bullet.transform.rotation);
        bullet.GetComponent<Bullet>().SetData(bulletDamage, transform.forward);
    }

    [ContextMenu("Debug Shoot")]
    public void Split()
    {
        Shoot();
    }
}
