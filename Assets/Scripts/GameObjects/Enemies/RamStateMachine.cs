﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RamStateMachine : EnemyStateMachine
{
    [SerializeField]
    private LayerMask objectMask;
    // Use this for initialization

    public override void MoveForward()
    {
        StartCoroutine(MyDelayCoroutine());
    }

    private IEnumerator MyDelayCoroutine()
    {
        yield return new WaitForSeconds(0.8f);

        RaycastHit hit;
        Vector3 pos = transform.position;
        pos.y = 0.5f;
        if (Physics.Raycast(pos, transform.forward, out hit, 1f, objectMask, QueryTriggerInteraction.Ignore))
        {
            GameObject target = hit.collider.gameObject;
            pos.z--;
            if (Physics.Raycast(pos, transform.forward, out hit, 1f, objectMask, QueryTriggerInteraction.Ignore))
            {
                targetPosition = Mathf.Round(transform.position.z - 1f);
            }
            else
                targetPosition = Mathf.Round(transform.position.z - 3f);
        }
        else targetPosition = Mathf.Round(transform.position.z - 2f);
        ChangeState(EEnemyState.Walk);
    }
}
