﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float m_speed;
    [SerializeField]
    private GameObject m_hitVFX;

    private int myDamage;
    private Vector3 myDirection;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BallDestroyer>())
        {
            if (m_hitVFX)
            {
                Instantiate(m_hitVFX, transform.position, m_hitVFX.transform.rotation);
            }
            SceneManagerScript.Instance.player.GetComponent<Health>().TakeDamage(gameObject, myDamage);
            Destroy(gameObject, Time.deltaTime);
        }
    }

    public void SetData(int damage, Vector3 direction)
    {
        myDamage = damage;
        myDirection = direction;
        StartCoroutine(MoveCoroutine());
    }

    private IEnumerator MoveCoroutine()
    {
        while (true)
        {
            transform.Translate(myDirection * Time.deltaTime * m_speed);
            yield return null;
        }
    }
}
