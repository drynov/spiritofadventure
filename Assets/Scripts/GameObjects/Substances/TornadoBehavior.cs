﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoBehavior : SubstanceBehavior
{
    [SerializeField]
    private int m_leftBorder;
    [SerializeField]
    private int m_rightBorder;
    [SerializeField]
    private int m_topBorder;
    [SerializeField]
    private int m_botBorder;
    [SerializeField]
    private float speed = 2;

    private IEnumerator myCoroutine;
    private List<Vector3> PossiblePositions = new List<Vector3>();

    private Vector3 newPosition;


    // Use this for initialization
    void Start()
    {
        RegisterSubstance();
        GameStateMachine.Instance.OnOtherObjectAction += MoveToPosition;
        GameStateMachine.Instance.OnEnemyTurnAction += UnReady;
    }

    private void OnDestroy()
    {
        SceneObjectsManager.Instance.UnRegistrate(this);
        GameStateMachine.Instance.OnOtherObjectAction -= MoveToPosition;
        GameStateMachine.Instance.OnEnemyTurnAction -= UnReady;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BallBehavior>())
        {
            Rigidbody rig = other.GetComponent<Rigidbody>();
            int angle = Random.Range(0, 360);
            Vector3 velocity = rig.velocity;
            velocity = Quaternion.AngleAxis(angle, Vector3.up) * velocity;
            rig.velocity = velocity;
        }
    }

    private void UnReady()
    {
        isReady = false;
    }

    private void FindNewPosition()
    {
        PossiblePositions.Clear();
        newPosition = Vector3.zero;
        FindFirstStagePositions();
        FindSecondStagePositions();
        if (PossiblePositions.Count <= 0)
            return;
        else
        {
            int nm = Random.Range(0, PossiblePositions.Count - 1);
            newPosition = PossiblePositions[nm];
        }
    }

    private void MoveToPosition()
    {
        if (myCoroutine != null)
            return;

        FindNewPosition();

        if (newPosition == Vector3.zero)
        {
            isReady = true;
            //SceneObjectsManager.Instance.UpdateSubstancesStatus();
            return;
        }

        myCoroutine = MoveCoroutine(newPosition);
        StartCoroutine(myCoroutine);
    }

    private IEnumerator MoveCoroutine(Vector3 pos)
    {
        pos.y = 0;
        Vector3 direction = transform.position - pos;
        float sqrDistance = direction.sqrMagnitude;
        while (sqrDistance > 0.01f)
        {
            transform.Translate(direction.normalized * Time.deltaTime * speed);
            sqrDistance = (pos - transform.position).sqrMagnitude;
            yield return null;
        }
        transform.position = pos;
        isReady = true;
        SceneObjectsManager.Instance.UpdateSubstancesStatus();
        myCoroutine = null;
    }

    public override bool ReadyCheck()
    {
        return isReady;
    }

    public override void RegisterSubstance()
    {
        SceneObjectsManager.Instance.Registrate(this);
    }

    private void FindFirstStagePositions()
    {
        // find free positions on first cirlce of tiles
        List<Vector3> FirstStagePositions = new List<Vector3>();
        for (int i = -1; i <= 1; i++)
        {
            int newX = Mathf.RoundToInt(transform.position.x + i);
            if (newX < m_leftBorder || newX > m_rightBorder)
                continue;
            for (int j = -1; j <= 1; j++)
            {
                int newZ = Mathf.RoundToInt(transform.position.z + j);
                if (newZ < m_botBorder || newZ > m_topBorder)
                    continue;
                FirstStagePositions.Add(new Vector3(newX, 0.5f, newZ));
            }
        }

        for (int i = 0; i < FirstStagePositions.Count; i++)
        {
            if (Physics.OverlapSphere(FirstStagePositions[i], 0.2f).Length <= 0)
            {
                PossiblePositions.Add(FirstStagePositions[i]);
            }
        }
    }

    private void FindSecondStagePositions()
    {
        // find free positions on second cirlce of tiles
        List<Vector3> SecondStagePositions = new List<Vector3>();
        for (int i = -2; i <= 2; i += 4)
        {
            int newX = Mathf.RoundToInt(transform.position.x + i);
            if (newX < m_leftBorder || newX > m_rightBorder)
                continue;
            for (int j = -2; j <= 2; j++)
            {
                int newZ = Mathf.RoundToInt(transform.position.z + j);
                if (newZ < m_botBorder || newZ > m_topBorder)
                    continue;
                SecondStagePositions.Add(new Vector3(newX, 0.5f, newZ));
            }
        }

        for (int i = -1; i <= 1; i++)
        {
            int newX = Mathf.RoundToInt(transform.position.x + i);
            if (newX < m_leftBorder || newX > m_rightBorder)
                continue;
            for (int j = -2; j <= 2; j += 4)
            {
                int newZ = Mathf.RoundToInt(transform.position.z + j);
                if (newZ < m_botBorder || newZ > m_topBorder)
                    continue;
                SecondStagePositions.Add(new Vector3(newX, 0.5f, newZ));
            }
        }

        for (int i = 0; i < SecondStagePositions.Count; i++)
        {
            if (Physics.OverlapSphere(SecondStagePositions[i], 0.2f).Length <= 0)
            {
                PossiblePositions.Add(SecondStagePositions[i]);
            }
        }
    }
}
