﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyedObject : MonoBehaviour
{
    [SerializeField]
    private Rigidbody[] m_Fragments;
    [SerializeField]
    private float m_AdditionalForce = 2f;
    [SerializeField]
    private bool m_ProtectSpeed;
    [SerializeField]
    private float m_MaxSpeed = 1f;

    private void Start()
    {
        if (m_Fragments == null || m_Fragments.Length == 0)
        {
            GetFragmentsFromChilds();
        }

        Vector3 dir = Vector3.zero;
        foreach (Rigidbody rig in m_Fragments)
        {
            dir = (rig.transform.position - transform.position).normalized;
            dir.y = Mathf.Clamp(dir.y, 0.1f, 1f);
            rig.AddForce(dir * m_AdditionalForce, ForceMode.Impulse);

            float x = Random.Range(-3f, 3f);
            float y = Random.Range(-3f, 3f);
            float z = Random.Range(-3f, 3f);

            rig.AddTorque(new Vector3(x, y, z));
        }

        StartCoroutine(HideFragments());

        if(m_ProtectSpeed)
        {
            StartCoroutine(ProtectSpeed());
        }
    }

    [ContextMenu("Get Childs Fragments")]
    public void GetFragmentsFromChilds()
    {
        m_Fragments = transform.GetComponentsInChildren<Rigidbody>();
    }

    private IEnumerator HideFragments()
    {
        yield return new WaitForSeconds(1.5f);

        foreach (Rigidbody rig in m_Fragments)
        {
            Destroy(rig.gameObject.GetComponent<Collider>());
            rig.isKinematic = true;
        }

        float timer = 0f;
        while (timer < 1f)
        {
            foreach (Rigidbody rig in m_Fragments)
            {
                rig.transform.Translate(Vector3.down * Time.deltaTime * 0.35f, Space.World);
            }

            timer += Time.deltaTime;
            yield return null;
        }

        Destroy(gameObject);
    }

    private IEnumerator ProtectSpeed()
    {
        Vector3 velocity = Vector3.zero;
        while (true)
        {
            foreach (var rig in m_Fragments)
            {
                velocity = rig.velocity;
                if(velocity.magnitude > m_MaxSpeed)
                {
                    velocity = velocity.normalized * m_MaxSpeed;
                    rig.velocity = velocity;
                }
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
