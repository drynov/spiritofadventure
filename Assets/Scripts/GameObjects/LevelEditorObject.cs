﻿using UnityEngine;

public class LevelEditorObject : MonoBehaviour
{
    public bool isDestroyable;
    public string Marker;
    public Vector3 size;


    private void Reset()
    {
        if (GetComponent<Collider>())
            size = GetComponent<Collider>().bounds.size;
    }
}