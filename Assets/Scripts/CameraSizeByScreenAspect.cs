﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSizeByScreenAspect : MonoBehaviour
{
    private Camera mainCamera;

    private Dictionary<float, float> sizes = new Dictionary<float, float>
    {
        {0.5f,  6.65f },
        {0.56f, 6f },
        {0.62f, 6.25f },
        {0.75f, 6.65f }
    };

	// Use this for initialization
	void Start () 
    {
        mainCamera = GetComponent<Camera>();
        if(!mainCamera)
        {
            Destroy(this);
            return;
        }

        mainCamera.orthographicSize = CalculateCameraSize();
	}

#if UNITY_EDITOR
	void Update ()
    {
        mainCamera.orthographicSize = CalculateCameraSize();
    }
#endif

    private float CalculateCameraSize()
    {
        float size = 6f;
        float aspectDiff = 999f;
        float currentAspect = (float)Screen.width / Screen.height;

        foreach (var item in sizes)
        {
            float diff = Mathf.Abs(currentAspect - item.Key);
            if (diff < aspectDiff)
            {
                aspectDiff = diff;
                size = item.Value;
            }
        }

        return size;
    }
}
